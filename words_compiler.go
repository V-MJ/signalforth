package main

import ("fmt"
	"strings"
)

//compilers
func (con *inerpreterContext) immidiate(){
	con.Dictionary[len(con.Dictionary)-1].Immediate = true;
}
func (con *inerpreterContext) enti(){
	con.state = 0;
}
func (con *inerpreterContext) entc(){
	con.state = -1;
}
func (con *inerpreterContext) recurse(){
	con.compileVal(int64(len(con.Dictionary)));
}
func (con *inerpreterContext) spstate(){
	con.Push(con.state);
}
func (con *inerpreterContext) putcomp(){
	con.compileVal(con.Pop());
}
func (con *inerpreterContext) execute(){
	executiontoken := con.Pop();
	//run the forth defined code
	if con.Dictionary[executiontoken].Function == nil{
		con.RPush();
		for con.InstructionPointer = con.Dictionary[executiontoken].Wmp; con.memory[con.InstructionPointer] != 999999; con.InstructionPointer++{ 
			con.Push(con.memory[con.InstructionPointer]);
			con.execute();
		}
		con.RPop();
	} else {//just run the machine function and return no need to modify returnstack
		con.Dictionary[executiontoken].Function();
	}
}
func (con *inerpreterContext) tick(){//find execution token of a word
	symbol := con.token();
	ext := findWord(con.Dictionary, symbol);
	if ext != -1 {
		con.Push(ext);
		return
	}
	fmt.Println("no such word as ->", symbol, "<-");
	con.abort();
}
func (con *inerpreterContext) semi(){
	con.compileVal(999999);
	con.Dictionary = append(con.Dictionary, con.tempWord);
	con.state = 0;
}
func (con *inerpreterContext) colon(){
	//clear the struct
	con.tempWord.Word = con.token();
	//con.tempWord.Word = con.retoken();
	con.tempWord.Immediate = false;
	con.tempWord.Wmp = con.here;
	//con.tempWord.Wmp = con.memory[con.here];
	//update state
	con.state = -1;
	//get name
	con.tempWord.Function = nil;	//this is nil for words defined in forth
}

func (con *inerpreterContext) body(){
	con.Push(con.Dictionary[con.Pop()].Wmp);
}

func (con *inerpreterContext) create(){
	//str := token();
	con.colon();
	
	ltr := findWord(con.Dictionary, "LIT");
	
	con.compileVal(ltr);
	con.compileVal(con.here+2);
	//con.compileVal(con.memory[con.here]+2);
	con.semi();

}

func (con *inerpreterContext) find(){
	addr := con.Pop();
	//construct string
	var token strings.Builder;
	for i := int64(0); con.memory[addr+i] != 0; i++ {
		token.WriteRune(rune(con.memory[addr+i]));
	}
	//find word
	wrd := findWord(con.Dictionary, token.String());
	if wrd != -1 {
		con.Push(wrd);
		if con.Dictionary[wrd].Immediate {
			con.Push(1);
		} else{
			con.Push(-1);
		}
	} else {
		con.Push(addr);
		con.Push(0);
	}
}

func (con *inerpreterContext) postpone(){
	word := con.token();
	//word := con.retoken();
	index := findWord(con.Dictionary, word);
	if con.Dictionary[index].Function == nil {
	for i := con.Dictionary[index].Wmp; con.memory[i] != 999999; i++{
		con.compileVal(con.memory[i]);
	}}
}

func (con *inerpreterContext) does(){
	con.Push(con.memory[con.here-1]);
	//can.Push(con.memory[con.memory[here]-1])
}

func (con *inerpreterContext) consant(){
	con.colon();
	con.compileVal(findWord(con.Dictionary, "LIT"));
	con.compileVal(con.Pop());
	con.semi();
}
