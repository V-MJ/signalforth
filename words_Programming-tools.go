package main

import "fmt"

func (con *inerpreterContext) dump(){
	rep := con.Pop();
	addr := con.Pop();
	for i := int64(0); i != rep; i++{
		fmt.Printf("%d ", con.memory[addr+i]);
	}
	fmt.Printf("\n");
}
//SEE
func (con *inerpreterContext) see(){
	str := con.token();
	//str := can.retoken();

	//find word
	wrd := findWord(con.Dictionary, str);
	//var wrd int64;
	/*for i := len(con.Dictionary)-1; i >= 0; i-- {
	if str == con.Dictionary[i].Word {
		wrd = int64(i);
		break;
	}}*/

	//check if builtin
	if con.Dictionary[wrd].Function != nil { fmt.Printf("builtin\n"); return; }

	//give word listing
	for i := con.Dictionary[wrd].Wmp; con.memory[i] != 999999; i++{
		if len(con.Dictionary)-1 < int(con.memory[i]) || con.memory[i] < 0 {
			i++; fmt.Printf("%d ", con.memory[i]);
		}else{
			fmt.Printf("%s ", con.Dictionary[con.memory[i]].Word);
			if con.Dictionary[con.memory[i]].Word == "LIT" || con.Dictionary[con.memory[i]].Word == "0BRANCH" || con.Dictionary[con.memory[i]].Word == "BRANCH" {
				i++; fmt.Printf("%d ", con.memory[i]);
			}
		}
	}
	fmt.Printf("\n");
}
