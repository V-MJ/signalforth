package main

import "fmt"

//out+in
func (con *inerpreterContext) dot(){
	fmt.Printf("%d\n", con.Pop());
}
func (con *inerpreterContext) udot(){
	fmt.Println(uint64(con.Pop()));
}
func (con *inerpreterContext) emit(){
	fmt.Printf("%s", string(con.Pop()));
}
func (con *inerpreterContext) qkey(){
	x := int64(0);
	if len(con.KeyBuffer) > 0{
		x = -1;
	}
	con.Push(x);
}
func (con *inerpreterContext) key(){
	r := <- con.KeyBuffer;
	//r := con.inchar();
	con.Push(int64(r));
}
func (con *inerpreterContext) source(){//add mwmory write
	/*con.Push(con.here);
	//con.Push(int64(len(con.KeyBuffer)));
	tmp := int64(0);
	//for i := <- con.KeyBuffer; i != '\n'; i = <- con.KeyBuffer{
	for i := con.inkey(); i != '\n'; i = con.inkey(){
		con.memory[con.here+tmp] = int64(i);
		tmp++;
	}
	con.memory[con.here+tmp] = '\n'; tmp++;
	con.memory[con.here+tmp] = 0;
	con.Push(tmp);*/
	//calculate chars in inbuffer
	var i int64;
	for i = 2; con.memory[i] != 0; i++{}
	con.Push(2);//where our inpubuffer begins
	con.Push(i-2);//calculated length of inputbuffer
}
func (con *inerpreterContext) words(){
	for i := 0; i <= len(con.Dictionary) - 1; i++{
		fmt.Printf("%d\t%s\t%d\n", i, con.Dictionary[i].Word, con.Dictionary[i].Wmp);
	}
	fmt.Printf("\n");
}
func (con *inerpreterContext) dotS(){
	fmt.Println(con.DataStack);
}
func (con *inerpreterContext) rDotS(){
	fmt.Println(con.ReturnStack);
}
func (con *inerpreterContext) accept(){
	max := con.Pop();
	adr := con.Pop();
	tru := int64(0);
	for tru=tru; tru < max; tru++ {
		//chr := <- con.KeyBuffer;
		chr := con.inkey();
		if chr == '\n'{ break; }
		con.memory[adr+tru] = int64(chr);
	}
	con.Push(tru);
}

func (con *inerpreterContext) lincom(){
	//for i := <- con.KeyBuffer; i != '\n'; i = <- con.KeyBuffer{}
	for i := con.inkey(); i != '\n'; i = con.inkey(){}
}
func (con *inerpreterContext) mlincom(){
	//for i := <- con.KeyBuffer; i != ')'; i = <- con.KeyBuffer{}
	for i := con.inkey(); i != ')'; i = con.inkey(){}
}

//>IN
func (con *inerpreterContext) invar(){
	con.Push(con.memory[con.inoffset]);
}