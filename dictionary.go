package main
//import "fmt"

type DictionaryEntry struct {
	Word string;		//name of this word like . + emit or xlerb
	Function func();	//native function if implemented in go
	Immediate bool;		//if this is true then this is allways executed
	Wmp int64;		//word memory pointer aka where the builtin is in memory
}

func (con *inerpreterContext) AppendDictionaryBuiltin(name string, function func(), immediate bool){
	var tmpEnt DictionaryEntry;
	tmpEnt.Word=name;
	tmpEnt.Function=function;
	tmpEnt.Immediate=immediate;
	tmpEnt.Wmp=-5			//where in memory for execution in this case its not in memory to catch some bugs
	con.Dictionary = append(con.Dictionary, tmpEnt);
}

//setup the dictionary
func (con *inerpreterContext) initDictionaryBuiltins(){
	//datastack 
	con.AppendDictionaryBuiltin("DROP",		con.drop,	false);
	con.AppendDictionaryBuiltin("2DROP",		con.drop2,	false);
	con.AppendDictionaryBuiltin("PICK",		con.pick,	false);
	con.AppendDictionaryBuiltin("ROLL",		con.roll,	false);
	con.AppendDictionaryBuiltin("DEPTH",		con.sdepth,	false);
	//returnstack
	con.AppendDictionaryBuiltin(">R",		con.trs,	false);
	con.AppendDictionaryBuiltin("R@",		con.rat,	false);
	con.AppendDictionaryBuiltin("R>",		con.rsf,	false);
	con.AppendDictionaryBuiltin("EXIT",		con.exitbag,	false);
	//memory
	con.AppendDictionaryBuiltin("!",		con.poke,	false);
	con.AppendDictionaryBuiltin("@",		con.peak,	false);
	con.AppendDictionaryBuiltin("HERE",		con.herp,	false);
	con.AppendDictionaryBuiltin("ALLOT",		con.allot,	false);
	con.AppendDictionaryBuiltin("MOVE",		con.move,	false);
	//math
	con.AppendDictionaryBuiltin("+",		con.add,	false);
	con.AppendDictionaryBuiltin("-",		con.sub,	false);
	con.AppendDictionaryBuiltin("*",		con.mul,	false);
	con.AppendDictionaryBuiltin("/",		con.div,	false);
	con.AppendDictionaryBuiltin("MOD",		con.mod,	false);
	con.AppendDictionaryBuiltin("FM/MOD",		con.fmmod,	false);
	con.AppendDictionaryBuiltin("UM/MOD",		con.ummod,	false);
	con.AppendDictionaryBuiltin("UM*",		con.umstar,	false);
	con.AppendDictionaryBuiltin("MOD",		con.smrem,	false);
	//double cell numbers
	con.AppendDictionaryBuiltin("S>D",		con.sctdc,	false);
	//out+in
	con.AppendDictionaryBuiltin(".",		con.dot,	false);
	con.AppendDictionaryBuiltin("U.",		con.udot,	false);
	con.AppendDictionaryBuiltin("EMIT",		con.emit,	false);
	con.AppendDictionaryBuiltin("KEY",		con.key,	false);
	con.AppendDictionaryBuiltin("?KEY",		con.qkey,	false);
	con.AppendDictionaryBuiltin("SOURCE",		con.source,	false);
	con.AppendDictionaryBuiltin("ACCEPT",		con.accept,	false);
	//numeric output
	con.AppendDictionaryBuiltin("<#",		con.ltrisuaita,	true);
	con.AppendDictionaryBuiltin("#",		con.risuaita,	true);
	con.AppendDictionaryBuiltin("#S",		con.ristaitas,	true);
	con.AppendDictionaryBuiltin("HOLD",		con.hold,	true);
	con.AppendDictionaryBuiltin("SIGN",		con.sign,	true);
	con.AppendDictionaryBuiltin("#>",		con.risuaitamt,	true);
	//interpreter
	con.AppendDictionaryBuiltin(":",		con.colon,	false);
	con.AppendDictionaryBuiltin("VARIABLE",		con.defvar,	false);
	con.AppendDictionaryBuiltin("LIT",		con.lit,	false);
	con.AppendDictionaryBuiltin("ABORT",		con.abort,	false);
	con.AppendDictionaryBuiltin("ABORT\"",		con.abortstr,	true);
	con.AppendDictionaryBuiltin("QUIT",		con.quit,	false);
	//comparison
	con.AppendDictionaryBuiltin("0=",		con.eq0,	false);
	con.AppendDictionaryBuiltin("0<",		con.lt0,	false);
	con.AppendDictionaryBuiltin("<",		con.lt,		false);
	con.AppendDictionaryBuiltin("U<",		con.ult,	false);
	con.AppendDictionaryBuiltin("=",		con.eq,		false);
	con.AppendDictionaryBuiltin(">",		con.mt,		false);
	//branching
	con.AppendDictionaryBuiltin("BRANCH",		con.branch,	false);
	con.AppendDictionaryBuiltin("BRANCH?",		con.branchq,	false);
	con.AppendDictionaryBuiltin("0BRANCH",		con.zbranch,	false);
	//debug/dev help
	con.AppendDictionaryBuiltin("WORDS",		con.words,	false);
	con.AppendDictionaryBuiltin("R.S",		con.rDotS,	false);
	con.AppendDictionaryBuiltin("\\",		con.lincom,	true);
	con.AppendDictionaryBuiltin("(",		con.mlincom,	true);
	//compiler
	con.AppendDictionaryBuiltin("IMMEDIATE",	con.immidiate,	false);
	con.AppendDictionaryBuiltin("EXECUTE",		con.execute,	false);
	con.AppendDictionaryBuiltin("'",		con.tick,	false);
	con.AppendDictionaryBuiltin(",",		con.putcomp,	false);
	con.AppendDictionaryBuiltin("[']",		con.lit,	false);
	con.AppendDictionaryBuiltin("[",		con.enti,	true);
	con.AppendDictionaryBuiltin("]",		con.entc,	true);
	con.AppendDictionaryBuiltin("RECURSE",		con.recurse,	true);
	con.AppendDictionaryBuiltin(";",		con.semi,	true);
	con.AppendDictionaryBuiltin(">BODY",		con.body,	false);
	con.AppendDictionaryBuiltin("EVALUATE",		con.eval,	false);
	con.AppendDictionaryBuiltin("CREATE",		con.create,	false);
	con.AppendDictionaryBuiltin("FIND",		con.find,	false);
	con.AppendDictionaryBuiltin("POSTPONE",		con.postpone,	true);
	con.AppendDictionaryBuiltin("DOES>",		con.does,	false)
	con.AppendDictionaryBuiltin("CONSTANT",		con.consant,	false);
	//bitwise and shifts
	con.AppendDictionaryBuiltin("AND",		con.sand,	false);
	con.AppendDictionaryBuiltin("OR",		con.sor,	false);
	con.AppendDictionaryBuiltin("XOR", 		con.sxor,	false);
	con.AppendDictionaryBuiltin("LSHIFT",		con.sshiftl,	false);
	con.AppendDictionaryBuiltin("RSHIFT",		con.sshiftr,	false);
	//chars and strings
	con.AppendDictionaryBuiltin("[CHAR]",		con.litchars,	true);
	con.AppendDictionaryBuiltin("CHAR",		con.chars,	false);
	con.AppendDictionaryBuiltin(".\"",		con.litstr,	true);
	con.AppendDictionaryBuiltin("WORD",		con.word,	false);
	con.AppendDictionaryBuiltin("COUNT",		con.count,	false);
	con.AppendDictionaryBuiltin("S\"",		con.strquot,	true);
	con.AppendDictionaryBuiltin("TYPE",		con.typescrn,	false);
	con.AppendDictionaryBuiltin("ENVIRONMENT?",	con.envq,	false);
	con.AppendDictionaryBuiltin("FILL",		con.fill,	false);
	//programming-tools
	con.AppendDictionaryBuiltin(".S",		con.dotS,	false);
	con.AppendDictionaryBuiltin("STATE",		con.spstate,	false);
	con.AppendDictionaryBuiltin("DUMP",		con.dump,	false);
	con.AppendDictionaryBuiltin("SEE",		con.see,	false);
	//programming-tools_ext
	con.AppendDictionaryBuiltin("BYE",		bye,		false);
	//strings wordset
	con.AppendDictionaryBuiltin("COMPARE",		con.compare,	false);
}