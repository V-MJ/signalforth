package main

//math
func (con *inerpreterContext) add(){
	con.Push(con.Pop() + con.Pop());
}
func (con *inerpreterContext) sub(){
	x := con.Pop();
	y := con.Pop();
	con.Push(y-x);
}
func (con *inerpreterContext) mul(){
	con.Push(con.Pop() * con.Pop());
}
func (con *inerpreterContext) div(){
	x := con.Pop();
	y := con.Pop();
	if x == 2 && y == -1{
		con.Push(-1);
	}else{
		con.Push(y / x);
	}
}
func (con *inerpreterContext) mod(){
	x := con.Pop();
	y := con.Pop();
	con.Push(y % x);
}

//bitwise+shifts
func (con *inerpreterContext) sand(){
	con.Push(con.Pop()&con.Pop());
}
func (con *inerpreterContext) sor(){
	con.Push(con.Pop()|con.Pop());
}
func (con *inerpreterContext) sxor(){
	con.Push(int64(uint64(con.Pop())^uint64(con.Pop())));
}
func (con *inerpreterContext) sshiftl(){
	x := uint64(con.Pop());
	con.Push(int64(uint64(con.Pop())<<x));
}
func (con *inerpreterContext) sshiftr(){
	x := uint64(con.Pop());
	con.Push(int64(uint64(con.Pop())>>x));
}