# SignalForth

A forth like language implemented in go.

Not under active development.

Great example of how to NOT use slices in go.

I'll propably rewrite this in C later.

This is **NOT** standards complient. In fact some words are intentionally implemented wrong.

Works on linux. Not tested on windows.

## Building

```
go build .
```

## Running

```
./signalforth
```

There is no output... don't worry just start typing something and press enter.

I haven't tested this but you might be able to write code into a file and then pipe it to signalforth.

## Examples

As forth is just a fancy RPN calculator you can do math with it

```
5 4 + .
```
The above code would print 9 to the screen.

You can make functions like this

```
\ this function prints *
: star 42 emit ;
```

Functions can contain loops and other functions

```
\ print n number of *
: nstars
	0 do
	star
	loop
;
\use like this "3 nstars"
```

There are also controll flow words like IF THEN and ELSE though I'm not sure if those works properly.

You can list all defined words by typing WORDS.

Oh btw type BYE to close the program.

## License

AGPLv3
