: DUP 0 PICK ;
: OVER 1 PICK ;
: ROT 2 ROLL ;
: SWAP 1 ROLL ;
: 2DUP OVER OVER ;
\ : 2DROP DROP DROP ;

: BASE 0 ;

: 1+ 1 + ;
: CELL+ 1+ ;

: INVERT -1 XOR ;

: IF ['] 0BRANCH , HERE 0 , ; IMMEDIATE
: THEN DUP HERE SWAP - SWAP ! ; IMMEDIATE
: ELSE ['] BRANCH , HERE 0 , SWAP DUP HERE SWAP - SWAP ! ; IMMEDIATE
: ITST IF 43 . THEN 78 . ;

: BEGIN HERE ; IMMEDIATE
: UNTIL ['] 0BRANCH , HERE - , ; IMMEDIATE
: TLP BEGIN DUP 1 + DUP 5 > UNTIL ;

\ : +LOOP SWAP ['] BRANCH , HERE - , DUP HERE SWAP ! 2 + SWAP ! ; IMMEDIATE

\ take 3 a direck rst port
: DO ( n n -- R -- loop-sys)
    ['] SWAP , ['] >R , ['] >R ,                    \ put to returnstack
    HERE                                            \ loop begins here
    ['] R> , ['] R> , ['] 2DUP , ['] >R , ['] >R ,  \ copy loopsys to stack
   \ ['] r.s , ['] .s ,
    ['] < , ['] 0BRANCH , HERE 0 ,                  \ jump out of loop in inden > limit
; IMMEDIATE
: LOOP
   \ ['] r.s , ['] .s ,
    ['] R> , ['] 1+ , ['] >R ,                      \ index incremenet
    ['] BRANCH , SWAP HERE - ,             \ go to the begining of loop
    DUP HERE SWAP - swap !                          \ update the 0branch returnlocation
; IMMEDIATE
: +LOOP
   \ ['] r.s , ['] .s ,
    ['] R> , ['] + , ['] >R ,                      \ index incremenet
    ['] BRANCH , SWAP HERE - ,             \ go to the begining of loop
    DUP HERE SWAP - swap !                          \ update the 0branch returnlocation
; IMMEDIATE

\ take 2
\ : LOOP+ ( n -- )
\     \ add n to loop index
\     R> + >R
\     \ branch to do
\     ['] BRANCH ,
\     \ compleate the branch
\     SWAP DUP HERE - SWAP !
\     \ update previous 0branch
\     DUP HERE SWAP - SWAP !
\ ; IMMEDIATE

: LEAVE 2DROP EXIT ;
: UNLOOP 2DROP ;

\ : I DUP ;
: J OVER ;
: I R> R> DUP >R SWAP >R ;
\ : J >R DUP R> ;

: AGAIN ['] BRANCH , HERE - , ; IMMEDIATE
: WHILE ['] 0BRANCH , HERE 0 , ; IMMEDIATE
: REPEAT ['] BRANCH , SWAP HERE - , DUP HERE SWAP - SWAP ! ; IMMEDIATE


\ : DO HERE 0 , ; IMMEDIATE

\ : INVERT -1 XOR ;
: CR 10 EMIT ;
: 1- 1 - ;
\ : 2/ 1 RSHIFT ;
: 2/ 2 / ;
: 2* 2 * ;
\ : 2* 1 LSHIFT ;
: 2@ DUP 1+ @ SWAP @ ;
: 2OVER 3 PICK 3 PICK ;
: 2SWAP ROT >R ROT R> ;
: ?DUP DUP IF DUP THEN ;
: NEGATE 0 SWAP - ;
: ABS DUP 0< IF NEGATE THEN ;
: BL 32 EMIT ;
: SPACE BL EMIT ;
: CELLS 1 * ;
: SPACES DUP 0= IF 1 -  BL SPACES THEN ;
: ABS DUP 0< IF NEGATE THEN ;

: LITERAL ['] LIT , ' , ; IMMEDIATE
\ : CONSTANT ['] LIT , ' , ; IMMEDIATE

: MIN -9223372036854775808 ;
: MAX 9223372036854775807 ;

: 2! SWAP OVER ! CELL+ ! ;
: /MOD 2DUP MOD ROT / ;
: */ ROT ROT * / ;
: */MOD */ MOD ;
: ALIGN 0 DROP ;
: ALIGNED DUP DROP ;

\ : ( KEY [CHAR] ) = INVERT IF RECURSE THEN ; IMMEDIATE

: C! ! ;
: C, 1 ALLOT C! ;
: C@ @ ;
: CHARS DUP DROP ;
: CHAR+ 1+ ;

: +! DUP @ ROT + SWAP ! ;

: ABS DUP 0< IF NEGATE THEN ;

: BASE 0 ;
: DECIMAL 10 BASE ! ;

: M* * ;

: TAB ABORT" PATE EI TUONUT POSTIA :(" ;

: >IN 1 ;

: >NUMBER DROP DROP DROP 5 0 5 ;




\ core_ext
: TUCK SWAP OVER ;
: NIP SWAP DROP ;
: TRUE -1 ;
: FALSE 0 ;
: <> XOR -1 0 ROT SWAP IF SWAP THEN DROP ;


: HEX 16 BASE ! ;

: REFILL -1 ;



\ PROGRAMMING TOOLS
: ? @ . ;

\ test the following    begin while if else then repeat until
: [ELSE] ( -- )
    1 BEGIN                                     
       BEGIN BL WORD COUNT DUP WHILE                
         2DUP S" [IF]" COMPARE 0= IF                
             2DROP 1+                              
          ELSE                                       
            2DUP S" [ELSE]" COMPARE 0= IF            
                2DROP 1- DUP IF 1+ THEN              
            ELSE                                      
                S" [THEN]" COMPARE 0= IF             
                   1-                                
               THEN
             THEN
          THEN ?DUP 0= IF EXIT THEN                   
       REPEAT 2DROP                                   
   REFILL 0= UNTIL                                   
    DROP
; IMMEDIATE
: [IF] ( flag -- ) 0= IF POSTPONE [ELSE] THEN ; IMMEDIATE
: [THEN] ( -- ) 0 DROP ; IMMEDIATE
: [UNDEFINED] BL WORD FIND NIP 0= ; IMMEDIATE
 
