package main

func (con *inerpreterContext) fmmod(){
	x := con.Pop();
	y := con.Pop();
	con.Push(y%x);
	con.Push(y/x);
}

func (con *inerpreterContext) ummod(){
	x := uint64(con.Pop());
	y := uint64(con.Pop());
	con.Push(int64(y%x));
	con.Push(int64(y/x));
}

func (con *inerpreterContext) umstar(){
	con.Push(int64(uint64(con.Pop())*uint64(con.Pop())));
}

func (con *inerpreterContext) smrem(){
	x := con.Pop();
	y := con.Pop();
	con.Push(y%x);
	con.Push(y/x);
}

func (con *inerpreterContext) sctdc(){
	x := con.Pop();
	con.Push(x);
	if x<0{
		con.Push(-1);
	}else{
		con.Push(0);
	}
}



//numeric thing
func (con *inerpreterContext) risuaita(){
	con.Push(con.Pop()/con.base);
}

func (con *inerpreterContext) ltrisuaita(){
	//compile branch
	con.compileVal(findWord(con.Dictionary, "BRANCH"));
	//con.Push(findWord(con.Dictionary, "BRANCH")); con.putcomp();
	//get here
	con.Push(con.here);
	//compile tmp address
	con.compileVal(0);
	//con.Push(int64(0)); con.putcomp();
}

func (con *inerpreterContext) ristaitas(){
	con.Push(0);
}

func (con *inerpreterContext) hold(){
	//compile the char
	con.compileVal(con.Pop());
	//con.putcomp();
}

func (con *inerpreterContext) sign(){
	if con.Pop() < 0{
		//compile -
		//con.Push(int64('-')); con.putcomp();
		con.compileVal(int64('-'));
	}
}

func (con *inerpreterContext) risuaitamt(){
	//get here
	x := con.here;
	//update branch location
	y := con.Pop();
	con.memory[y] = x - y;
	//get lit
	litti := findWord(con.Dictionary, "LIT");
	//compile lit
	con.compileVal(litti);
	//con.Push(litti); con.putcomp();
	//compile string addr
	con.compileVal(x+1);
	//con.Push(x+1); con.putcomp();
	//compile lit
	con.compileVal(litti);
	//con.Push(litti); con.putcomp();
	//push string length
	con.compileVal(y-(x+1));
	//con.Push(y-(x+1)); con.putcomp();
}