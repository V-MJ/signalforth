package main

import ("os"
	"fmt"
	"strings"
	"strconv"
	"bufio"
	_"embed"
	"time"
)

//include standard forth library for core standars comliancy
//also I just wanted to use embed as it's new and cool
//go:embed core.fs
var core string;


type inerpreterContext struct {
	InstructionPointer int64;	//where in the word we at
	tempWord DictionaryEntry;	//global word being compiled
	here int64;			//pointer to freecell in mem thhis is apparrently correct
	base int64;			//pointer to mem where base is "numeric base"
	inoffset int64;			//where parse offset is stored
	memory [65535]int64;		//memory to peek and poke at
	state int64;			//-1=compile, 0=execution 
	Dictionary []DictionaryEntry;
	KeyBuffer chan rune;//, 128;

	ReturnStack []int64;
	DataStack []int64;
}
//need to allocate memory space
//0 = base
//1 = inoffset
//2-? = inputbuffer
//1-32 words
//32-63 pad
//64-95 inputbuffer
//96 here
//97 base
//98 state

//usefull functions
func (con *inerpreterContext) compileVal(val int64){
	con.memory[con.here] = val;
	con.here++;
}
func findWord(dict []DictionaryEntry, w2f string) int64 {
	var dictionaryOffset int64 = -1;//default value for a unnest 
	for i := len(dict)-1; i >= 0; i-- {
	if strings.EqualFold(w2f, dict[i].Word){
		dictionaryOffset = int64(i);
		break;
	}}
	return dictionaryOffset;
}
func iswhitespace(r rune)bool{
	if r == ' ' || r == '\t' || r == '\n' { return true; }
	return false;
}
//update to use proper base
func Number( input string, base int64 ) (bool, int64) {
	x, err := strconv.ParseInt(input, int(base), 64);
	if err != nil {
		fmt.Printf("not a number ->%s<- \n", input);
		os.Exit(1);
	}
	return true, x;
}

func (con *inerpreterContext) otoken() string{
	//var token string;
	var r rune;
	//flush white spaces
	for r = <- con.KeyBuffer; iswhitespace(r); r = <- con.KeyBuffer {}
	//build a string
	var token strings.Builder;
	for r = r; !iswhitespace(r); r = <- con.KeyBuffer {
		token.WriteRune(r);
	}
	return token.String();
}

func (con *inerpreterContext) token() string{
	//clear whitespace
	var r rune;
	for r = con.inkey(); iswhitespace(r); r = con.inkey() {}
	//parse a token from inputfuffer
	var token strings.Builder;
	for r = r; !iswhitespace(r); r = con.inkey() {
		token.WriteRune(r);
	}
	return token.String();
}
func (con *inerpreterContext) inkey() rune {
	if con.memory[2+con.memory[con.inoffset]] == 0 { con.inrefill(); }
	r := int64(con.memory[2+con.memory[con.inoffset]]);
	con.memory[con.inoffset]++;
	return rune(r);
}
func (con *inerpreterContext) inrefill(){
	//clear and refill the input buffer
	for i:=0; i<128; i++{
		con.memory[2+i] = 0;
		r := <- con.KeyBuffer;
		if r == '\n'{
			con.memory[2+i] = int64('\n');
			i++;
			con.memory[2+i] = 0;
			break;
		}
		con.memory[2+i] = int64(r);
	}
	con.memory[con.inoffset] = 0;
}

//to be implemented using words
func (con *inerpreterContext) Interpret(){//now with 100% more compile
	//interpret loop
	for {
		w := con.token();
		if w == ""{//incase a token can't be found
			//fill the inputbuffer with data
			continue;//restart the loop
		}
		cont := true;
		for i := len(con.Dictionary)-1; i >= 0; i-- {
			if strings.EqualFold(w, con.Dictionary[i].Word){
				if con.Dictionary[i].Immediate == true || con.state == 0 {
					con.Push(int64(i));
					con.execute();
				} else {
					con.compileVal(int64(i));
					i = 0;
				}
				cont = false;
			}
		}
		if cont && con.state == -1 {
		//if w == con.tempWord.Word {
		if strings.EqualFold(w, con.tempWord.Word){
			con.compileVal(int64(len(con.Dictionary)));
			cont = false;
		}}
		if cont {
			bul, nub := Number(w, con.memory[con.base]);
			if con.state == -1 && bul {
				//get lit
				con.compileVal(findWord(con.Dictionary, "LIT"));
				//compile number
				con.compileVal(nub);
			} else{
				con.Push(nub);
			}
		}
		/*if len(con.KeyBuffer) == 0 {
			fmt.Printf(" ok\n> ");
		}*/
	}
}

func (con *inerpreterContext) loadString( file string ){
	line := 0;
	line++;
	for _, r := range file {
		con.KeyBuffer <- r;
		if r == '\n'{
			time.Sleep(time.Millisecond*50);
			line++;
		}
	}
	con.KeyBuffer <- ' '; //this fixes an oldbug and is here for sanity and its not like exstra whitespace would break my program...
}

func main(){
	var context inerpreterContext;
	context.KeyBuffer = make(chan rune, 128);
	//position 0 of mem is for base
	context.base = 0;
	context.memory[context.base] = 10;
	//allocate base
	context.here = context.base + 1;
	//allocate and define the >in
	context.inoffset = 1;
	context.memory[context.inoffset] = 0;
	context.here += 1;
	//allocate word buffer
	context.here += 128;
	//note it's up to the user to write code thats less than 128 chars per row

	//init interpreter context
	context.initDictionaryBuiltins();
	//start a interpreter "thread"
	go context.Interpret();

	//send core librery to the interpreter	
	context.loadString(core);

	//accept input from stdin
	for {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		context.loadString(text);
		context.KeyBuffer <- '\n';
	}
}
