package main

import "strings"

func (con *inerpreterContext) compare(){
	s1l := con.Pop();
	s1a := con.Pop();
	s2l := con.Pop();
	s2a := con.Pop();

	//s1 builder
	var s1 strings.Builder;
	for i := int64(0); i != s1l; i++ {
		s1.WriteRune(rune(con.memory[s1a+i]));
	}

	//s2 builder
	var s2 strings.Builder;
	for i := int64(0); i != s2l; i++ {
		s2.WriteRune(rune(con.memory[s2a+i]));
	}

	//compleat string compare
	if s1.String() == s2.String(){
		con.Push(0); return;
	}
	//partial compare
	if strings.HasPrefix(s1.String(), s2.String()) || strings.HasPrefix(s2.String(), s1.String()){
		con.Push(-1); return;
	}
	//no similarities
	con.Push(1);
}