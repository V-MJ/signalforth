package main

//stack
func (con *inerpreterContext) drop(){
	con.Pop();
}
func (con *inerpreterContext) drop2(){
	con.Pop();
	con.Pop();
}
func (con *inerpreterContext) lit(){
	con.InstructionPointer++;
	con.Push(con.memory[con.InstructionPointer]);
}
func (con *inerpreterContext) pick(){
	x := con.Pop();
	y := int64(len(con.DataStack)-1);
	con.Push(con.DataStack[y-x]);
}
func (con *inerpreterContext) roll(){//1 roll = swap, 2 roll = rot
	//get values
	x := int64(len(con.DataStack)-2) - con.Pop();
	y := con.DataStack[x];
	//modify stack
	con.DataStack = append(con.DataStack[:x], con.DataStack[x+1:]...);
	con.Push(y);
}
func (con *inerpreterContext) sdepth(){
	con.Push(int64(len(con.DataStack)));
}

//returnstack
func (con *inerpreterContext) trs(){//>R
	x := con.InstructionPointer;
	con.InstructionPointer = con.Pop();
	con.RPush();
	con.InstructionPointer = x;
}
func (con *inerpreterContext) rat(){//R@
	con.Push(con.InstructionPointer);
	con.RPop();
}
func (con *inerpreterContext) rsf(){//R>
	x := con.InstructionPointer;
	con.RPop();
	con.Push(con.InstructionPointer);
	con.InstructionPointer = x;
}
func (con *inerpreterContext) exitbag(){
	con.RPop();
}

//memory
func (con *inerpreterContext) peak(){
	con.Push(con.memory[con.Pop()]);
}
func (con *inerpreterContext) poke(){
	x := con.Pop();
	y := con.Pop();
	con.memory[x] = y;
}
func (con *inerpreterContext) herp(){
	con.Push(con.here);
}
func (con *inerpreterContext) allot(){
	con.here += con.Pop();
}

//strings
func (con *inerpreterContext) word(){
	x := con.Pop();//get the char
	var r rune;
	memsl := con.here;
	//flush white spaces
	//for r = <- con.KeyBuffer; iswhitespace(r); r = <- con.KeyBuffer {}
	//for r = r; r != rune(x); r = <- con.KeyBuffer{
	for r = con.inkey(); iswhitespace(r); r = con.inkey() {}
	for r = r; r != rune(x); r = con.inkey(){
		con.memory[con.here] = int64(r);
		con.here++;
	}
	con.memory[con.here] = 0;
	con.here++;
	con.Push(memsl);
}
func (con *inerpreterContext) count(){
	x := con.Pop();
	var leng int64;
	for i := x; con.memory[i] != 0; i++ {
		leng++;
	}
	con.Push(x);
	con.Push(leng);

}
func (con *inerpreterContext) strquot(){
	//get and compile branch
	con.compileVal(findWord(con.Dictionary, "BRANCH"));
	//get here
	x := con.here;
	//compile temp here
	con.compileVal(0);
	//string and its length
	var r rune;
	//for r = <- con.KeyBuffer; iswhitespace(r); r = <- con.KeyBuffer {}
	for r = con.inkey(); iswhitespace(r); r = con.inkey() {}
	meml := con.here;
	var strlen int64;
	for r = r; r != '"'; r = con.inkey(){
	//for r = r; r != '"'; r = con.inchar(){
		con.memory[con.here] = int64(r);
		con.here++;
		strlen++;
	}
	con.memory[con.here] = 0;
	con.here++;
	//update branch jump
	con.memory[x]=con.here-x-1;
	//get lit
	litr := findWord(con.Dictionary, "LIT");
	
	//compile the stuff
	con.compileVal(litr);
	con.compileVal(meml);
	con.compileVal(litr);
	con.compileVal(strlen);
}
func (con *inerpreterContext) typescrn(){
	leng := con.Pop();
	addr := con.Pop();
	for i := int64(0); i < leng; i++ {
		con.Push(con.memory[addr+i]);
		con.emit();
	}
}
func (con *inerpreterContext) abortstr(){
	//get text
	//compile it
	con.strquot();
	//compile rot <- get the thing in front
	con.compileVal(findWord(con.Dictionary, "ROT"));
	//find 0branch
	con.compileVal(findWord(con.Dictionary, "0BRANCH"));
	//get here and compile tmp value
	hop := con.here;
	con.compileVal(0);
	//print text()type
	con.compileVal(findWord(con.Dictionary, "TYPE"));
	//abort
	con.compileVal(findWord(con.Dictionary, "ABORT"));

	//get here
	dest := con.here;
	//update 0branch
	con.memory[hop] = dest-hop;

	//2drop
	drp := findWord(con.Dictionary, "DROP");
	con.compileVal(drp);
	con.compileVal(drp);
}

func (con *inerpreterContext) fill(){
	chr := con.Pop();
	len := con.Pop();
	adr := con.Pop();
	for i := int64(0); i != len; i++{
		con.memory[adr+i] = chr;
	}
}

func (con *inerpreterContext) eval(){
	len := con.Pop();
	adr := con.Pop();
	for i := int64(0); i != len; i++{
		con.KeyBuffer <- rune(con.memory[adr+i]);
	}
}

func (con *inerpreterContext) move(){
	rang := con.Pop();
	adr2 := con.Pop();
	adr1 := con.Pop();
	for i := int64(0); i < rang; i++ {
		con.memory[adr2+i] = con.memory[adr1+i];
	}
}