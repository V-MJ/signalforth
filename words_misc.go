package main

import ("os"
	"fmt"
	"strings"
)

func bye(){
	os.Exit(0);
}

//comparision
func (con *inerpreterContext) lt0(){
	x := int64(0);
	if con.Pop() < 0 {
		x = -1;
	}
	con.Push(x);
}
func (con *inerpreterContext) eq0(){
	x := int64(0);
	if con.Pop() == 0{
		x = -1
	}
	con.Push(x);
}
func (con *inerpreterContext) lt(){
	x := con.Pop();
	y := con.Pop();
	z := int64(0);
	if y < x{
		z = -1;
	}
	con.Push(z);
}
func (con *inerpreterContext) ult(){
	x := uint64(con.Pop());
	y := uint64(con.Pop());
	z := int64(0);
	if y < x{
		z = -1;
	}
	con.Push(z);
}
func (con *inerpreterContext) eq(){
	x := con.Pop();
	y := con.Pop();
	z := int64(0);
	if y == x{
		z = -1;
	}
	con.Push(z);
}
func (con *inerpreterContext) mt(){
	x := con.Pop();
	y := con.Pop();
	z := int64(0);
	if y > x{
		z = -1;
	}
	con.Push(z);
}
//jumps/branching
func (con *inerpreterContext) branch(){
	con.InstructionPointer++;
	con.InstructionPointer += con.memory[con.InstructionPointer]-1;
}
func (con *inerpreterContext) branchq(){
	con.InstructionPointer++;//increment ip to get the point where to jump
	if con.Pop() < 0 {
		con.InstructionPointer += con.memory[con.InstructionPointer]-1;
	}
}
func (con *inerpreterContext) zbranch(){
	con.InstructionPointer++;
	x := con.Pop();
	if x == 0 {
		con.InstructionPointer += con.memory[con.InstructionPointer]-1;
	}
}

//chars and strins
func (con *inerpreterContext) litchars(){
	wrd := con.token();
	con.compileVal(findWord(con.Dictionary, "LIT"));
	con.compileVal(int64(wrd[0]));
}
func (con *inerpreterContext) chars(){
	wrd := con.token();
	con.Push(int64(wrd[0]));
}
func (con *inerpreterContext) litstr(){
	ltr := findWord(con.Dictionary, "LIT");
	out := findWord(con.Dictionary, "EMIT");
	//r := <- con.KeyBuffer;
	r := con.inkey();
	for r=r; r != '"'; r = con.inkey(){
		con.compileVal(ltr);
		con.compileVal(int64(r));
		con.compileVal(out);
	}
}

//environment?
func (con *inerpreterContext) envq(){
	//get the string data
	length := con.Pop();
	location := con.Pop();

	//build a string
	var text strings.Builder;
	for i := int64(0); i != length; i++ {
		text.WriteRune(rune(con.memory[location+i]));
	}
	str := text.String();
	fmt.Println(str);


	//switchcase  for the following or return 0
	switch str{
		case "/COUNTED-STRING" :
			con.Push(31);
			con.Push(-1);
			break;
		case "ADDRESS-UNIT-BITS" :
			con.Push(64);
			con.Push(-1);
			break;
		case "FLOORED" :
			con.Push(-1);
			con.Push(-1);
			break;
		case "MAX-CHAR" :
			con.Push(-1);
			con.Push(-1);
			break;
		case "MAX-D" :
			con.Push(31);
			con.Push(-1);
			break;
		case "MAX-N" :
			con.Push((8*8*8*8*8*8*8*8)/2);
			con.Push(-1);
			break;
		case "MAX-U" :
			con.Push(-1);
			con.Push(-1);
			break;
		case "MAX-UD" :
			con.Push(-1);
			con.Push(-1);
			con.Push(-1);
			break;
		case "RETURN-STACK-CELLS" :
			con.Push(128);
			con.Push(-1);
			break;
		case "STACK-CELLS" :
			con.Push(64);
			con.Push(-1);
			break;
		default :
			con.Push(0);
			break;
	}
}

func (con *inerpreterContext) defvar(){
	ltr := findWord(con.Dictionary, "LIT");
	name := con.token();
	//allocate from memory
	mem := con.here;
	con.here++;
	con.Push(1);
	con.allot();
	var de = new(DictionaryEntry);
	de.Word = name;
	de.Wmp = con.here;
	con.compileVal(ltr);
	con.compileVal(mem);
	con.compileVal(999999);
	con.Dictionary = append(con.Dictionary, *de);
}

//aborting
func (con *inerpreterContext) abort(){
	for i := len(con.DataStack); i > 0; i--{
		con.Pop();
	}
	con.quit();
}
//func abortquot(){}
func (con *inerpreterContext) quit(){
	for i := len(con.ReturnStack); i > 0; i--{
		con.RPop();
	}
	con.InstructionPointer = 0;
}